package org.dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleValuesForRegister {
	
	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com");
		driver.manage().window().maximize();
		File f=new File("/home/elamparithi/Documents/Testdata/Testdata1.xls");
		FileInputStream fis=new FileInputStream(f);
		Workbook book=Workbook.getWorkbook(fis);
		Sheet sheet = book.getSheet("Register");
		int rows = sheet.getRows();
		int columns = sheet.getColumns();
		for (int i = 1; i < rows; i++) {
			String firstname = sheet.getCell(0,i).getContents();
			String lastname = sheet.getCell(1,i).getContents();
			String email = sheet.getCell(2,i).getContents();
			String pass = sheet.getCell(3,i).getContents();
			
			driver.findElement(By.xpath("//a[text()='Register']")).click();
			 driver.findElement(By.id("gender-male")).click();
		        driver.findElement(By.id("FirstName")).sendKeys(firstname);
		        driver.findElement(By.id("LastName")).sendKeys(lastname);
		        driver.findElement(By.id("Email")).sendKeys(email);
		        driver.findElement(By.id("Password")).sendKeys(pass);
		        driver.findElement(By.id("ConfirmPassword")).sendKeys(pass);
		        driver.findElement(By.id("register-button")).click();
		        for (int j= 0; j < columns; j++) {
		        	String cell = sheet.getCell(j,i).getContents();
		        	System.out.println("The inputs are :" + cell);
					
				}
			
		}
		
	}

}
